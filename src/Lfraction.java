import java.math.BigDecimal;
import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      double var1 = 453.975;
      double frac1 = .4;
      long var2 = (long)var1;
      System.out.println(frac1*7/1);
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if ((Objects.isNull(a)) || (Objects.isNull(b))){
         throw new IllegalArgumentException("Numerator " + a + " or denominator " + b + "is null");
      }
      if (b == 0 ){
         throw new IllegalArgumentException("Denominator " + b + " is invalid. Denominator must not be zero.");
      } else{
         long gcd = Math.abs(gcd(a, b));
         if (b < 0){
            numerator = -a/gcd;
            denominator = -b/gcd;
         } else{
            numerator = a/gcd;
            denominator = b/gcd;
         }
      }
   }

   // Greatest common demoniator function
   private static long gcd(long a, long b){
      return b == 0 ? a : gcd(b, a % b);
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      String numstring = Long.toString(numerator);
      String denomstring = Long.toString(denominator);
      String fracstring = numstring + "/" + denomstring;
      return fracstring;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      long num1 = ((Lfraction)m).getNumerator();
      long denum1 = ((Lfraction)m).getDenominator();
      if ((num1 == numerator) && (denum1 == denominator)){
         return true;
      } else{
         return false;
      }
   }

   /** Hashcode has to be the same for equal fractions and in general, different
    * for different fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      int hash = toString().hashCode();
      return hash;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long newnum = m.getNumerator() * getDenominator() + getNumerator() * m.getDenominator();
      long newdenom = m.getDenominator() * getDenominator();
      Lfraction result = new Lfraction(newnum, newdenom);
      return result;
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long newnum = m.getNumerator() * getNumerator();
      long newdenom = m.getDenominator() * getDenominator();
      Lfraction result = new Lfraction(newnum, newdenom);
      return result;
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (getNumerator() == 0){
         throw new IllegalArgumentException("Cannot inverse " + this.toString() + " (Cannot divide by zero)");
      }
      Lfraction result = new Lfraction(getDenominator(), getNumerator());
      return result;
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      long newnum = -1 * getNumerator();
      Lfraction result = new Lfraction(newnum, getDenominator());
      return result;
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      long newnum =  (getNumerator() * m.getDenominator()) - (m.getNumerator() * getDenominator());
      long newdenom = m.getDenominator() * getDenominator();
      Lfraction result = new Lfraction(newnum, newdenom);
      return result;
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.getNumerator() == 0){
         throw new IllegalArgumentException("Cannot divide by 0");
      }
      long newdenom = m.getNumerator() * getDenominator(); // Division is multiplication with the other operand reversed
      long newnum = m.getDenominator() * getNumerator();
      Lfraction result = new Lfraction(newnum, newdenom);
      return result;
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      long num1 = getNumerator() * m.getDenominator();
      long num2 = m.getNumerator() * getDenominator();
      if (num1 > num2){
         return 1;
      } else if (num1 == num2){
         return 0;
      } else {
         return -1;
      }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(getNumerator(), getDenominator()); // TODO!!!
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      if (Math.abs(getNumerator()) < getDenominator()){
         return 0L;
      }
      int numnospare = (int)Math.floor((getNumerator()/getDenominator()));
      return numnospare;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() { // -33, 3
      long numeratorr = 0;
      if (Math.abs(getNumerator()) > getDenominator()){
         int numnospare = (int)Math.floor((getNumerator()/getDenominator()));
         numeratorr = getNumerator() - numnospare * getDenominator(); // -33 - (-11)*3
      } else{
         return new Lfraction(getNumerator(), getDenominator());
      }
      return new Lfraction(numeratorr, getDenominator());
   }

   /** Approximate value of the fraction.
    * @return real value of this fraction
    */
   public double toDouble() {
      int num = (int)getNumerator();
      int denom = (int)getDenominator();
      double result = (double) num/denom;
      return result;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      long fullint = (long)f;
      double decimal = f - fullint;
      long numeratorpart = Math.round((decimal * d)/1);
      Lfraction result = new Lfraction(fullint * d + numeratorpart, d);
      return result;
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String split[] = s.split("/");
      char last = s.charAt(s.length()-1);
      if (last == '/'){
         throw new IllegalArgumentException("String " + s + " to convert must not end with /");
      }
      if ((split.length > 2) && (split[2] != null)){
         throw new IndexOutOfBoundsException("Too many arguments in string " + s + " to be converted to fraction");
      }
      for (String a : split){
         if (!isNumeric(a)){
            throw new IllegalArgumentException("Illegal argument " + a + " found in " + s);
         }
      }
      long numerator = Long.valueOf(split[0]);
      long denom = Long.valueOf(split[1]);
      return new Lfraction(numerator, denom);
   }

   private static boolean isNumeric(String strNum) { //https://www.baeldung.com/java-check-string-number
      if (strNum == null) {
         return false;
      }
      try {
         double d = Double.parseDouble(strNum);
      } catch (NumberFormatException nfe) {
         return false;
      }
      return true;
   }

   public Lfraction pow(int power) { //Tests are uploaded to bitbucket https://bitbucket.org/gugret/hwa4/src/master/test/LfractionTest.java
      if (power == 0) {
         return new Lfraction(1, 1);
      } else if (power == 1) {
         return new Lfraction(getNumerator(), getDenominator());
      } else if (power == -1) {
         return this.inverse();
      } else if (power > 1) {
         return times(pow(power - 1));
      } else{
         return pow(Math.abs(power)).inverse();
      }
   }
}

